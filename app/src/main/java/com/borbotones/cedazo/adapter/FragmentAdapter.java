package com.borbotones.cedazo.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.borbotones.cedazo.view.fragment.FilterFragment;
import com.borbotones.cedazo.view.fragment.PreferenceFragment;


public class FragmentAdapter extends FragmentPagerAdapter {
    private static final int FRAGMENT_NUMBER = 3;
    private static final String[] TAB_TITLES = {"FILTRO"
                                                , "PREFERENCIAS"};

    public FragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i)
        {
            case 0:
                return new FilterFragment();

            case 1:
                return new PreferenceFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return this.FRAGMENT_NUMBER;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return this.TAB_TITLES[position];
    }
}
