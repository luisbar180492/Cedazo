package com.borbotones.cedazo.server;

import com.borbotones.cedazo.model.User;


import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by luisbar on 07-05-16.
 */
public interface Requests {

    @GET("{id}/")
    Observable<User> getTarea(@Path("id") int id);

}
