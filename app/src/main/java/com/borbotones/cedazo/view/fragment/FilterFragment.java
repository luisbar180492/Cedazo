package com.borbotones.cedazo.view.fragment;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.borbotones.cedazo.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import co.hkm.soltag.TagContainerLayout;
import co.hkm.soltag.TagView;

public class FilterFragment extends Fragment implements TagView.OnTagClickListener {
    @Bind(R.id.tcl_fl_tags_container)TagContainerLayout tclFlTagsContainer;
    @Bind(R.id.b_ff_add_tag)Button bFfAddTag;
    @Bind(R.id.actv_ff_tag_name)AutoCompleteTextView actvFfTagName;

    public FilterFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_filter, null, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        tclFlTagsContainer.setOnTagClickListener(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick(R.id.b_ff_add_tag)
    void bFfAddTag() {
        tclFlTagsContainer.addTag(actvFfTagName.getText().toString());
    }

    @Override
    public void onTagClick(int position, String text) {
        tclFlTagsContainer.removeViewAt(position);
    }

    @Override
    public void onTagLongClick(int position, String text) {

    }
}
